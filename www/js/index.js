document.addEventListener('deviceready', onDeviceReady, false);

// this method is execute when the application is working
function onDeviceReady() {
    // Add the click event for adding an element
    document.getElementById('add-button').addEventListener('click', addTask);
    // Event listener on the ul element 
    document.getElementById('list-container').addEventListener('click', function(e) {eventClickHandler(e)});
    getBackData();
}

// Click fonction on the ul element 
eventClickHandler = (event) => {
    console.log(event);
    if (event.target.tagName === "LI") {
        event.target.classList.toggle("checked");
        storeData();
    } else if (event.target.tagName === "SPAN") {
        event.target.parentElement.remove();
        storeData();
    };
}

// Add task methode
function addTask() {
    // Search element needed to add a task
    const inputBox = document.getElementById('input-box');
    const listElement = document.getElementById('list-container');
    if (inputBox.value === '') {
        // Show alert if nothing is write
        alert('Write something to do !');
    } else {
        // create a new list item
        const newLiElement = document.createElement('li');
        newLiElement.innerHTML = inputBox.value;
        listElement.appendChild(newLiElement);
        let span = document.createElement('span');
        span.innerHTML = '\u00d7';
        newLiElement.appendChild(span);
    }
    // Emptied the input 
    inputBox.value = '';
    storeData();
}

function storeData () {
    const listElement = document.getElementById('list-container');
    NativeStorage.setItem("data",listElement.innerHTML, this.setSuccess, this.setError);
}

function setSuccess(obj) {
    console.log('SUCCESS Storing Data!!!');
        console.log(obj);
};
function setError(error) {
    console.log('ERROR Storing Data!!!');
        console.log(error.code);
        if (error.exception !== "") console.log(error.exception);
};

function getBackData() {
    NativeStorage.getItem("data", this.getSuccess, this.getError);
}

function getSuccess(obj) {
    console.log('SUCCESS Getting Data!!!');
    console.log(obj);
    const listElement = document.getElementById('list-container');
    listElement.innerHTML = obj;
};

function getError(error) {
    console.log('ERROR Getting data!!!');
    console.log(error.code);
};
